


import 'package:flutter/material.dart';
import 'package:practice_texts/texts/text1.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Text1(),
    );
  }
}