import 'package:flutter/material.dart';
import 'package:practice_texts/utils/button_style.dart';

import 'text3.dart';

class Text1 extends StatefulWidget {
  final String? texto3;
  const Text1({
    Key? key,
    this.texto3 = '',
  }) : super(key: key);

  @override
  _Text1State createState() => _Text1State();
}

class _Text1State extends State<Text1> {
  TextEditingController controller =
      TextEditingController(); //Para controlar el flujo de la accion del boton se define variable de tipo de la clase textediting controller

  @override
  Widget build(BuildContext context) {
    String valor = '';      //Para almacenar los caracteres almacenados en el value del onChanged
    bool bandera = false;   // Para determinar la logica de la presion del boton

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Screen One'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                controller: controller,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20.0)),
                  hintText: 'Ingrese Caracteres',
                  labelText: 'Password',
                ),
                onChanged: (value) {
                  valor = value;
                      
                  print( 'Contiene los caracteres que estan en la variable value $value');
                },
              ),
              const SizedBox(height: 40.0),
              TextButton(
                style: textButtonStyle,
                child: const Text('Press'),
                onPressed: () {
                setState(() {
                  bandera = true;
                  valor=  controller.text;
                  
                });
                if(valor.isNotEmpty) {
                  Navigator.push(context,
                   MaterialPageRoute(builder: (context) => Text3(texto: valor)));
                }else{
                  showDialog(
                    context: context, //Investigar sobre el context
                    builder: (context){
                      return AlertDialog (
                        title:  const Text('Por favor ingrese algun tipo de caracter'),
                        content: TextButton(
                          style: textButtonStyle,
                          child: const Text('Exit'),
                          onPressed: (){
                            Navigator.of(context).pop();
                          },
                        ),
                      );
                    }
                  );
                


                }
              }
              ),
              const SizedBox(height: 30.0),

              Text(
              controller.text,
                style: const TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w800,
                  color: Colors.black,
                ),
              ),
              Text(widget.texto3!)
            ],
          ),
        ),
      ),
    );
  }
}
