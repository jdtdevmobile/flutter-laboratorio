import 'package:flutter/material.dart';

final ButtonStyle textButtonStyle = TextButton.styleFrom(
  primary: Colors.white,
  backgroundColor: Colors.teal,
  minimumSize:  const Size(88, 40),
  padding: const EdgeInsets.symmetric(horizontal: 16.0),
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(16.0),
  )
);