import 'package:flutter/material.dart';
import 'package:practice_texts/Pages/main_pages.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'mainPage': (_) => const MainPage(),
};